import * as React from "react";
import { Routes, Route } from "react-router-dom";
import SignIn from "./components/Signin";
import CssBaseline from "@mui/material/CssBaseline";
import HeaderBar from "./components/HeaderBar";
import Product from "./components/Product";
import Cart from "./components/Cart";
import ProductList from "./components/ProductList";

function App() {
    return (
      <React.Fragment>
          <CssBaseline />
          <HeaderBar />
          <main>
              <Routes>
                  <Route path="/" element={<ProductList />} />
                  <Route path="/sign-in" element={<SignIn />} />
                  <Route path="/product/:productId" element={<Product />} />
                  <Route path="/cart" element={<Cart />} />
              </Routes>
          </main>
      </React.Fragment>
    );
}

export default App;
