import axios from "axios";

const httpClient = axios.create({
    baseURL: "http://localhost:8000",
    withCredentials: true,
});

export function login(email, password, onSuccess, onError) {
    httpClient.get("/sanctum/csrf-cookie").then(response => {

        httpClient.post("/api/auth/login", {email: email, password: password}).then(response => {
            onSuccess(response)
        }).catch(errors => {
            onError(errors)
        });
    });
}

export function logout(onSuccess) {
    httpClient.get("/api/auth/logout").then(_ => {
        onSuccess()
    }).catch(errors => {
        console.log(errors)
    });
}

export async function listProducts(page) {
    return (await httpClient.get(`/api/product?page=${page}`)).data;
}

export async function getProductById(id) {
    return (await httpClient.get(`/api/product/${id}`)).data;
}

export async function getCartByUserId(user_id) {
    return (await httpClient.get(`/api/user/${user_id}/cart`)).data;
}

export async function getLoggedUser() {
    try {
        const response = await httpClient.get("/api/user");
        if (response.status === 200)
        {
            return response.data
        }
    } catch (err) { }

    return null
}

export async function addCartItem(userId, productId, quantity) {
    return (await httpClient.post(`/api/user/${userId}/cart/item`, {
        'product_id': productId,
        'quantity': quantity,
    })).data;
}

export async function removeCartItem(userId, itemId) {
    return (await httpClient.delete(`/api/user/${userId}/cart/item/${itemId}`)).data;
}

export async function checkout(userId) {
    return (await httpClient.post(`/api/user/${userId}/cart/checkout`)).data;
}

