import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

import { useNavigate } from "react-router-dom";
import {logout} from "../clients/EcommerceClient";


export default function HeaderBar() {
    const navigate = useNavigate();

    const onLogout = async () => {
        await logout(function () {
            navigate("/")
        })
    }

    return (
        <AppBar position="relative">
            <Toolbar>
                <Typography variant="h6" color="inherit" noWrap>
                    Ecommerce
                </Typography>
                <Button color="inherit" onClick={() => navigate("/")} >Home</Button>
                <Button color="inherit" onClick={() => navigate("/cart")}>Go to Cart</Button>
                <Button color="inherit" onClick={() => navigate("/sign-in")}>Sign in</Button>
                <Button color="inherit" onClick={() => onLogout()}>Log out</Button>
            </Toolbar>
        </AppBar>
    );
}