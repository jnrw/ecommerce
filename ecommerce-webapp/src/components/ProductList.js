import * as React from 'react';
import ProductItem from "./ProductItem";
import {Grid, Pagination, Stack} from "@mui/material";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import {useEffect, useState} from "react";
import {listProducts} from "../clients/EcommerceClient";

export default function ProductList() {
    const [products, setProducts] = useState([])
    const [countPages, setCountPages] = useState(1)

    const getProducts = async (page=1) => {
        const data = await listProducts(page)
        setProducts(data.data)
        setCountPages(data.last_page)
    }

    useEffect(() => {
        getProducts()
    }, [])

    return (
        <Container sx={{ py: 0 }} maxWidth="md">
            <Typography py={8} variant="h3">Products</Typography>
            <Grid container spacing={4}>
                {products.map((product, index) => (
                    <ProductItem key={index} name={product.name} price={product.price} id={product.id} />
                ))}
            </Grid>

            <Stack spacing={2}>
                <Pagination count={countPages} onChange={(event, page) => getProducts(page)} />
            </Stack>

        </Container>
    );
}