import * as React from 'react';
import {Card, CardContent, CardMedia, Grid, Input, Button} from "@mui/material";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import {useEffect, useState} from "react";
import {addCartItem, getLoggedUser, getProductById} from "../clients/EcommerceClient";
import {useNavigate, useParams} from "react-router-dom";

export default function Product(props) {
    let params = useParams();
    const navigate = useNavigate();
    const [product, setProduct] = useState({})
    const [quantity, setQuantity] = useState(1)

    const onSetQuantityHandler = (event) => {
        setQuantity(event.target.value)
    }

    const getProduct = async () => {
        const data = await getProductById(params.productId)
        setProduct(data)
    }

    const onBuyHandler = async () => {
        const user = await getLoggedUser();
        if(user == null)
        {
            navigate("/sign-in")
        }
        await addCartItem(user.id, product.id, quantity)
        navigate("/cart")
    }

    useEffect(() => {
        getProduct()
    }, [])

    return (
        <Container sx={{ py: 8 }} maxWidth="md">
            <Grid container spacing={4}>
                <Grid p={1} xs={12} sm={6} md={4} >
                    <Card sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
                        <CardMedia component="img" image="https://images.unsplash.com/photo-1649452815651-77a262578b0e?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxfDB8MXxyYW5kb218MHx8fHx8fHx8MTY1MTQ2MTIyMQ&ixlib=rb-1.2.1&q=80&w=1080" />
                        <CardContent sx={{ flexGrow: 1 }}>
                            <Typography gutterBottom variant="h5" component="h2">
                                { product.name }
                            </Typography>
                            <Typography>
                                $ { product.price }
                            </Typography>

                            <Typography>
                                Quantity: <Input style={ {width: 80} } defaultValue={1} onInput={event => (onSetQuantityHandler(event))} type="number" min="1" />
                            </Typography>
                            <div>

                            </div>
                            <Button variant="contained" onClick={() => onBuyHandler()}>Buy</Button>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        </Container>
    );
}