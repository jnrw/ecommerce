import * as React from 'react';
import {Card, CardContent, CardMedia, Grid, Link} from "@mui/material";
import Typography from "@mui/material/Typography";

export default function ProductItem(props) {
    return (
        <Grid p={1} xs={12} sm={6} md={4}>
            <Link underline="none" href={`/product/${props.id}`}>
                <Card sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
                    <CardMedia component="img" image="https://images.unsplash.com/photo-1649452815651-77a262578b0e?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxfDB8MXxyYW5kb218MHx8fHx8fHx8MTY1MTQ2MTIyMQ&ixlib=rb-1.2.1&q=80&w=1080" />
                    <CardContent sx={{ flexGrow: 1 }}>
                        <Typography gutterBottom variant="h5" component="h2">
                            { props.name }
                        </Typography>
                        <Typography>
                            $ { props.price }
                        </Typography>
                    </CardContent>
                </Card>
            </Link>
        </Grid>
    );
}