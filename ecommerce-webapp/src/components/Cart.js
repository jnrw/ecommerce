import * as React from 'react';
import {useNavigate} from "react-router-dom";
import {useEffect, useState} from "react";
import {checkout, doCheckout, getCartByUserId, getLoggedUser, removeCartItem} from "../clients/EcommerceClient";
import Container from "@mui/material/Container";
import {Card, CardContent, CardMedia, Grid} from "@mui/material";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

export default function Cart() {
    const navigate = useNavigate();
    const [user, setUser] = useState({})
    const [cart, setCart] = useState({ items: [], total: 0.0 })

    const getCart = async () => {
        const userData = await getLoggedUser()
        if(userData == null)
        {
            navigate("/sign-in")
        }
        setUser(userData)
        const cartData = await getCartByUserId(userData.id)
        setCart(cartData)
    }

    const onRemoveItemHandler = async (itemId) => {
        await removeCartItem(user.id, itemId)
        getCart()
    }

    const onCheckoutHandler = () => {
        checkout(user.id)
        navigate("/")
    }

    useEffect(() => {
        getCart()
    }, [])

    return (
        <Container sx={{ py: 8 }} maxWidth="md">
            <Grid container spacing={4} sx={{flexDirection: 'column' }}>
                <Typography variant="h2" align="center">
                    Cart
                </Typography>
                    {cart.items.map((item, index) => (
                        <Grid key={index} p={1} xs={12} sm={12} md={12} >
                            <Card sx={{ display: 'flex' }}>
                                <CardMedia
                                    component="img"
                                    sx={{ width: 151 }}
                                    image="https://images.unsplash.com/photo-1649452815651-77a262578b0e?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxfDB8MXxyYW5kb218MHx8fHx8fHx8MTY1MTQ2MTIyMQ&ixlib=rb-1.2.1&q=80&w=1080"
                                />
                                <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                                    <CardContent sx={{ flex: '1 0 auto' }}>
                                        <Typography component="div" variant="h5">
                                            { item.product.name }
                                        </Typography>
                                        <Typography variant="div" color="text.secondary" component="div">
                                            Quantity: { item.quantity }
                                        </Typography>
                                        <Typography variant="div" color="text.secondary" component="div">
                                            Unit Price: $ { item.product.price }
                                        </Typography>
                                        <Button color="warning" variant="contained" onClick={() => onRemoveItemHandler(item.id)}>
                                            Remove
                                        </Button>
                                    </CardContent>
                                </Box>
                            </Card>
                        </Grid>
                    ))}
                <Typography mt={5} variant="h5">
                    Total: ${cart.total}
                </Typography>

                <Button variant="contained" onClick={() => onCheckoutHandler()}>
                    Checkout
                </Button>
            </Grid>
        </Container>
    );
}