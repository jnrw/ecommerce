@component('mail::message')

<table>
    <thead>
    <tr>
        <th>Product</th>
        <th>Quantity</th>
        <th>Unit price</th>
        <th>Total price</th>
    </tr>
    </thead>

    <tbody>
    @foreach($order->getItems() as $item)
        <tr>
            <td>{{ $item->getProductName() }}</td>
            <td>{{ $item->getProductQuantity() }}</td>
            <td>{{ $item->getProductUnitPrice() }}</td>
            <td>{{ $item->getProductTotal() }}</td>
        </tr>
    @endforeach
    </tbody>

    <tfoot>
        <tr>
            <td colspan="3">Total</td>
            <td>{{ $order->getTotal() }}</td>
        </tr>
    </tfoot>
</table>


