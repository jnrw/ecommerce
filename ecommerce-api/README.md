# Ecommerce API

## Running locally

```
docker-compose up
php artisan migrate
php artisan db:seed
php artisan db:seed --class=UserSeeder // optional
php artisan serve
```

The optional command `php artisan db:seed --class=UserSeeder` can be used to create a new user for testing purposes.

```
Username: user
Email: user@email.com
Password: password
```

