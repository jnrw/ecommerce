<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            ['name' => 'The Locked Door', 'price' => '3.99'],
            ['name' => 'The Dark One', 'price' => '4.99'],
            ['name' => 'Brutal Vows', 'price' => '3.99'],
            ['name' => 'Dream Town', 'price' => '14.99'],
            ['name' => 'Strangers We Know', 'price' => '4.99'],
            ['name' => 'Rules Of Prey', 'price' => '9.99'],
            ['name' => 'The Power of Positive Thinking', 'price' => '12.99'],
            ['name' => 'Regretting You', 'price' => '5.99'],
            ['name' => 'Bad Alibi', 'price' => '2.49'],
            ['name' => 'Lauren', 'price' => '0.99'],
            ['name' => 'Enola Holmes', 'price' => '8.49'],
            ['name' => 'The Sandman', 'price' => '23.55'],
            ['name' => 'The Bad Guy Movie', 'price' => '3.99'],
            ['name' => 'Roar', 'price' => '15.99'],
            ['name' => 'Conversation with Friends', 'price' => '12.99'],
            ['name' => 'King of Dreams', 'price' => '0.99'],
            ['name' => 'Relative Fortunes', 'price' => '0.99'],
            ['name' => 'An Empire Asunder', 'price' => '0.99'],
            ['name' => 'The Good Nurse', 'price' => '8.99'],
            ['name' => 'The Summer I Turned Pretty', 'price' => '8.99']
        ];

        Product::insert($products);
    }
}
