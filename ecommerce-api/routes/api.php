<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/login', [AuthController::class, 'login']);
Route::get('/auth/logout', [AuthController::class, 'logout']);
Route::post('/auth/token', [AuthController::class, 'token']);

Route::get('/product', [ProductController::class, 'index']);
Route::get('/product/{product_id}', [ProductController::class, 'show']);

Route::group(['middleware' => ['auth:sanctum']], function() {
   Route::get('/user/{user_id}/cart', [CartController::class, 'show']);
   Route::post('/user/{user_id}/cart/item', [CartController::class, 'store_item']);
   Route::delete('/user/{user_id}/cart/item/{cart_item_id}', [CartController::class, 'destroy_item']);
   Route::post('/user/{user_id}/cart/checkout', [CartController::class, 'checkout']);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
