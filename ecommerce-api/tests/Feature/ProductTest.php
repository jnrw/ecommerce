<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    public function test_product_get()
    {
        // Arrange
        $product = Product::factory()->create();
        $uri = "/api/product/$product->id";

        // Act
        $response = $this->get($uri);

        // Assert
        $response->assertOk();
        $response->assertJsonFragment(['id' => $product->id]);
    }

    public function test_product_get_not_found()
    {
        // Arrange
        $uri = "/api/product/1";

        // Act
        $response = $this->get($uri);

        // Assert
        $response->assertNotFound();
    }

    public function test_product_list_empty()
    {
        // Act
        $response = $this->get('/api/product');

        // Assert
        $response->assertOk();
        $response->assertJsonCount(0, 'data');
    }

    public function test_product_list_with_values()
    {
        // Arrange
        $products = Product::factory()->count(2)->create();

        // Act
        $response = $this->get('/api/product');

        // Assert
        $response->assertOk();
        $response->assertJsonCount($products->count(), 'data');
    }
}
