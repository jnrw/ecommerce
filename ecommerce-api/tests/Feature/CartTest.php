<?php

namespace Tests\Feature;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Product;
use Illuminate\Testing\Assert;
use Tests\TestCase;

class CartTest extends TestCase
{
    public function test_get_cart_unauthenticated()
    {
        // Arrange
        $cart = Cart::factory()->create();
        $uri = "/api/user/$cart->user_id/cart";

        // Act
        $response = $this->get($uri, ['Accept' => 'application/json']);

        // Assert
        $response->assertStatus(401);
    }

    public function test_get_cart_empty()
    {
        // Arrange
        $cart = Cart::factory()->create();
        $user = $cart->user;
        $uri = "/api/user/$cart->user_id/cart";

        // Act
        $response = $this
            ->actingAs($user)
            ->get($uri, ['Accept' => 'application/json']);

        // Assert
        $response->assertOk();
        $response->assertJsonCount(0, 'items');
    }

    public function test_cart_with_values()
    {
        // Arrange
        $cart = Cart::factory()->create();
        $cartItem = CartItem::factory()->create([
            'cart_id' => $cart,
            'quantity' => 1
        ]);
        $user = $cart->user;
        $uri = "/api/user/$user->id/cart";

        // Act
        $response = $this
            ->actingAs($user)
            ->get($uri, ['Accept' => 'application/json']);

        // Assert
        $response->assertJsonCount(1, 'items');
        $response->assertJsonFragment([
            'total' => floatval($cartItem->product->price)
        ]);
    }

    public function test_cart_post_item()
    {
        // Arrange
        $cart = Cart::factory()->create();
        $user = $cart->user;
        $product = Product::factory()->create();
        $uri = "/api/user/$cart->user_id/cart/item";
        $data = [
            'product_id' => $product->id,
            'quantity' => 1,
        ];

        // Act
        $response = $this
            ->actingAs($user)
            ->post($uri, $data, ['Accept' => 'application/json']);

        // Assert
        $response->assertCreated();
        Assert::assertIsNumeric($response->json('id'));
    }

    public function test_cart_delete_item()
    {
        // Arrange
        $cartItem = CartItem::factory()->create();
        $user = $cartItem->cart->user;
        $uri = "/api/user/$user->id/cart/item/$cartItem->id";

        // Act
        $response = $this
            ->actingAs($user)
            ->delete($uri, ['Accept' => 'application/json']);

        // Assert
        $response->assertOk();
    }
}
