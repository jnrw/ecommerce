<?php

namespace App\Services;

use App\Models\CartItem;

interface CartServiceInterface
{
    public function getCart($userId);
    public function addItem(int $cartId, int $productId, int $quantity): CartItem;
    public function removeItem(int $cartItemId);
    public function checkout(int $userId);
}
