<?php

namespace App\Services;

use App\Dtos\OrderDto;
use App\Dtos\OrderItemDto;
use App\Mail\OrderMail;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\User;
use App\Repository\CartRepositoryInterface;
use Illuminate\Support\Facades\Mail;

class CartService implements CartServiceInterface
{
    private CartRepositoryInterface $cartRepository;

    public function __construct(CartRepositoryInterface $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }
    public function getCart($userId)
    {
        return $this->cartRepository->getCartByUser($userId);
    }

    public function addItem(int $cartId, int $productId, int $quantity): CartItem
    {
        return $this->cartRepository->createCartItem([
            'cart_id' => $cartId,
            'product_id' => $productId,
            'quantity' => $quantity
        ]);
    }

    public function removeItem(int $cartItemId): int
    {
        return $this->cartRepository->deleteCartItemById($cartItemId);
    }

    private function clean_cart(Cart $cart)
    {
        $item_ids = array();
        foreach ($cart->items as $item)
        {
            array_push($item_ids, $item->id);
        }
        $this->cartRepository->deleteCartItemById($item_ids);
    }

    public function checkout(int $userId)
    {
        $user = User::find($userId);
        $cart = $this->cartRepository->getCartByUser($userId);

        $orderItems = array();

        foreach ($cart->items as $item) {
            $totalItem = $item->quantity * $item->product->price;
            $order = new OrderItemDto($item->product->name, $item->quantity, $item->product->price, $totalItem);
            array_push($orderItems, $order);
        }

        $order = new OrderDto($orderItems, $cart->total);
        Mail::to($user)->send(new OrderMail($order));
        $this->clean_cart($cart);
    }
}
