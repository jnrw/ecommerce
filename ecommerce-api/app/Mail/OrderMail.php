<?php

namespace App\Mail;

use App\Dtos\OrderDto;
use App\Models\Cart;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @param  OrderDto  $cart
     * @return void
     */
    protected OrderDto $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OrderDto $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.order')
            ->with([
                'order' => $this->order
            ]);
    }
}
