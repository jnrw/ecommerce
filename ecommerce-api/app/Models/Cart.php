<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id'
    ];

    protected $appends = ['total'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(CartItem::class);
    }

    protected function total(): Attribute
    {
        return new Attribute(
            function () {
                $total = 0;

                foreach ($this->items as $item) {
                    $total_item = $item->quantity * $item->product->price;
                    $total += $total_item;
                }

                return $total;
            }
        );
    }


}
