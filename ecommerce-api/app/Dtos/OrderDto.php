<?php

namespace App\Dtos;

class OrderDto
{
    public $_items;
    public $_total;

    public function __construct($_items, $_total)
    {
        $this->_items = $_items;
        $this->_total = $_total;
    }

    public function getItems()
    {
        return $this->_items;
    }

    public function getTotal()
    {
        return $this->_total;
    }
}
