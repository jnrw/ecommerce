<?php

namespace App\Dtos;

class OrderItemDto
{
    private $_productName;
    private $_productQuantity;
    private $_productUnitPrice;
    private $_productTotal;

    public function __construct($product_name, $product_quantity, $product_unit_price, $product_total)
    {
        $this->_productName = $product_name;
        $this->_productQuantity = $product_quantity;
        $this->_productUnitPrice = $product_unit_price;
        $this->_productTotal = $product_total;
    }

    public function getProductName()
    {
        return $this->_productName;
    }

    public function getProductQuantity()
    {
        return $this->_productQuantity;
    }

    public function getProductUnitPrice()
    {
        return $this->_productUnitPrice;
    }

    public function getProductTotal()
    {
        return $this->_productTotal;
    }



}
