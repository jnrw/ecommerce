<?php

namespace App\Http\Controllers;

use App\Services\CartServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    private CartServiceInterface $cartService;

    public function __construct(CartServiceInterface $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $user = Auth::user();

        if ($user->id == $user_id) {
            return $this->cartService->getCart($user_id);
        }

        return response([], 403);
    }

    public function store_item(Request $request, $user_id)
    {
        $validatedData = $request->validate([
            'product_id' => 'required|exists:\App\Models\Product,id',
            'quantity' => 'required|integer|gte:1'
        ]);

        $user = Auth::user();

        if ($user->id == $user_id) {
            $cart = $this->cartService->getCart($user_id);

            return $this->cartService->addItem(
                $cart->id,
                $validatedData['product_id'],
                $validatedData['quantity']
            );
        }

        return response([], 403);
    }

    public function destroy_item(Request $request, $user_id, $cart_item_id)
    {
        $request->merge([
            'cart_item_id' => $request->route('cart_item_id')
        ]);

        $this->validate($request, [
            'cart_item_id' => 'required|exists:\App\Models\CartItem,id',
        ]);

        $user = Auth::user();

        if ($user->id == $user_id) {
            return $this->cartService->removeItem($cart_item_id);
        }

        return response([], 403);
    }

    public function checkout(int $user_id)
    {
        $user = Auth::user();

        if ($user->id == $user_id) {
            return $this->cartService->checkout($user_id);
        }

        return response([], 403);
    }
}
