<?php

namespace App\Repository;

interface CartRepositoryInterface
{
    public function getCartByUser($userId);
    public function createCartItem($cartItem);
    public function deleteCartItemById($cartItemId);
}

