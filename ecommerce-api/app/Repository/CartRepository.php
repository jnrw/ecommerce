<?php

namespace App\Repository;

use App\Models\Cart;
use App\Models\CartItem;

class CartRepository implements CartRepositoryInterface
{

    public function getCartByUser($userId)
    {
        return Cart::with('items.product')->where('user_id', $userId)->first();
    }

    public function createCartItem($cartItem)
    {
        return CartItem::create($cartItem);
    }

    public function deleteCartItemById($cartItemId)
    {
        return CartItem::destroy($cartItemId);
    }
}
